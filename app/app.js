const express = require("express");
const app = express();
const port = 8000;

// Connecting with redis
const redis = require("redis");
const redis_port = 6379;
const redisClient = redis.createClient(redis_port);

// Connecting with MongoDB
const MongoClient = require('mongodb').MongoClient;
const mongo_url = "mongodb://localhost:27017/";

app.use(express.json());

// Get Redis data by id only
app.get("/get/:id", (req, res) => {
    const id = String(req.params.id);
    redisClient.get(id, (error, data) => {
        if (error) {
            res.status(400).send(error);
        }
        if (data) {
            res.status(200).send(JSON.parse(data));
            console.log(JSON.parse(data))
        } else {
            res.status(400).send({
                "Message": "Data not found!"
            });
        }
    })
})

// Get Redis all data
app.get("/get", (req, res) => {
    redisClient.keys("key-*", async (err, keys) => {
        let res_data = [];

        // console.log('keys::', keys)
        if (err) {
            res.status(400).send(err);
        }

        for (var i = 0, len = keys.length; i < len; i++) {
            redisClient.get(keys[i], (error, data) => {
                if (error) {
                    res.status(400).send(error);
                }
                const allData = JSON.parse(data);
                res_data.push(allData);
            })
        }
        setTimeout(() => {
            console.log('inside setTimeout::')
            console.log(res_data)
            res.send(res_data);
        }, 1000)
    })
})

// Get Mongo data by unique id only
app.get("/mongo/:id", (req, res) => {
    const id = req.params.id;

    MongoClient.connect(mongo_url, function (err, db) {
        if (err) throw err;
        const dbo = db.db("sensor_data");
        dbo.collection("data").findOne({_id:id}, function (err, result) {
            if (err) throw err;
            console.log(result);
            res.send(result);
            db.close();
        });
    });
});

// Get all Mongo data
app.get("/mongo", async (req, res) => {
    const mongoData = [];
    MongoClient.connect(mongo_url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("sensor_data");
        dbo.collection("data").find({}).toArray(function (err, result) {
            if (err) throw err;
            mongoData.push(result);
        });
    });

    setTimeout(() => {
        // console.log('inside setTimeout::')
        console.log(mongoData);
        res.send(mongoData);
    }, 1000)
})

app.listen(port, () => {
    console.log("Server running at ", port);
})
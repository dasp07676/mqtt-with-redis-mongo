No Dockerfile or docker-compose files are added yet.I am working on it.
To check the code you need to have Redis(Because without redis it will not store the data into Redis cache) and MongoDB Compass(Because without MongoDB compass we can't check how many data are stored) to be installed into your local machine.

For downloading Redis follow this -
    https://linuxize.com/post/how-to-install-and-configure-redis-on-ubuntu-18-04/

For downloading MongoDB Compass follow this -
    https://docs.mongodb.com/compass/current/install/

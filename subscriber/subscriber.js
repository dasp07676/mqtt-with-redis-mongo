const { json } = require("express");
const express = require("express");
const app = express();
var mqtt = require("mqtt");

// To check a mosquitto broker
// mosquitto_sub -p 1883 -h "lora_server.distronix.in" -t "#"

// For connecting Redis
const redis = require("redis");
const redis_port = 6379;
const redisClient = redis.createClient(redis_port);

// For mongo
const MongoClient = require('mongodb').MongoClient;
const mongo_url = "mongodb://localhost:27017/";

// Broker url
const url = "lora_server.distronix.in";

const brokerURL = "mqtt://" + url;
const options = {
    // username: "distronix",
    // password: "distronix",
    keepalive: 1,
    port: 1883,
    clean: false,
    reconnectPeriod: 500,
    clientId: "mqttjs_" + Math.random().toString(16).substr(2, 8)
};
// Connecting with broker
const client = mqtt.connect(brokerURL, options)

// Checking if subscriber is listening or not
client.on("connect", (err) => {
    if (!err) {
        console.log("No error!!!");
    }
    console.log("Is subscriber ready to recieve? " + client.connected);
})

// Subscribing to a particular topic.We can also do "#" for all topics...
client.subscribe("/dist/+/+/data/sens/", function (err) {
    if (err) {
        console.log("Error while getting the data..." + JSON.stringify(err));
    }
})

// If subscriber is online, subscriber will get some messages as response...
client.on("message", function (topic, message) {
    const jsondata = JSON.parse(message);

    // As postman doesn't allow a key to be like this "2/11/12" so first we need to split the timestamp and then add it to the key
    const timestamp = jsondata.node.timestamp;
    const resTimestamp = timestamp.split(",");
    const date = resTimestamp[0].split("/");
    const totalDate = `${date[0]}.${date[1]}.${date[2]}`
    const time = resTimestamp[1].split(":");
    const totalTime = `${time[0]}.${time[1]}.${time[2]}`;
    
    //Creating a random variable which will always give a random value.And it will help us to create a unique key 
    const random = Math.random();
    const totalTimeStamp = `Date-${totalDate}.Time-${totalTime}.Random-${random}`

    // If there is no "node" child in the json format we need to check that and also console that for better understanding
    if (jsondata.node == undefined) {
        console.log(jsondata);
    }
    // Creating a json format for storing the values in redis cache and also in mongodb
    else {
        const values = [{
            "_id": `key-${String(jsondata.node.id)}-${String(totalTimeStamp)}`,
            "Sensor-id": jsondata.node.id,
            "Timestamp": jsondata.node.timestamp,
            "Topic": topic,
            "Status": "Default"
        }];

        redisClient.set(`key-${String(jsondata.node.id)}-${String(totalTimeStamp)}`, JSON.stringify(values));

        // Inserting sensor data into Mongo
        MongoClient.connect(mongo_url, function (err, db) {
            if (err) throw err;
            const dbo = db.db("sensor_data");
            dbo.collection("data").insertMany(values, (err, res) => {
                if(err) throw err;
            });
        });

        console.log("---------------------------------------------------");
        // console.table(jsondata.node);    
        console.log(values);
        console.log("Data Inserted");
        // console.log({ jsondata, topic })
        console.log("---------------------------------------------------");
    }
});